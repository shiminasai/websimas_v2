from django.db import models
from otras.models import Autor
from publicaciones.models import Tematica
from ckeditor.fields import RichTextField
from ckeditor_uploader.fields import RichTextUploadingField
from django.template.defaultfilters import slugify
from sorl.thumbnail import ImageField
from websimas.utils import get_file_path

# Create your models here.

class Catblog(models.Model):
    catblog = models.CharField('Categoria del blog', max_length=350, blank=True, null=True)

    def __str__(self):
        return self.catblog

class Blog(models.Model):
    blog = models.CharField(max_length=500, blank=True, null=True)
    idautor = models.ForeignKey(Autor, verbose_name="Autor", blank=True, null=True, on_delete = models.CASCADE)
    fecha = models.DateField(blank=True, null=True)
    #descripcion = models.TextField(blank=True, null=True)
    foto = ImageField( upload_to=get_file_path, blank=True, null=True)
    credito = models.CharField(max_length=200, blank=True, null=True) 
    uri = models.CharField(max_length=500, blank=True, null=True, editable=False)
    idcatblog = models.ForeignKey('Catblog', verbose_name='Categoria del blog',blank=True, null=True, on_delete = models.CASCADE)   
    resumen = RichTextUploadingField(blank=True, null=True)
    tags = models.CharField(max_length=300, blank=True, null=True)
    tematica = models.ManyToManyField(Tematica, verbose_name='Tematica del blog', blank=True)

    fileDir = 'fotoBlogs/'

    def save(self, *args, **kwargs):  
        add = not self.pk
        super(Blog, self).save(*args, **kwargs)
        if add:
            self.uri = (slugify(self.blog)) + '-' + str(self.id)
            kwargs['force_insert'] = False # create() uses this, which causes error.
            super(Blog, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('blog_detalle', kwargs={'slug': self.uri})

    def __str__(self):
        return self.blog