from django.shortcuts import render, get_object_or_404
from django.views.generic import ListView
from blogs.models import *
from blogs.forms import TagForm
from otras.models import MenuFoto

# Create your views here.
class ListBlogsView(ListView):
    model = Blog
    queryset = Blog.objects.order_by('-fecha')
    # paginate_by = 10

    def get_context_data(self, **kwargs):
        context = super(ListBlogsView, self).get_context_data(**kwargs)
        context['form_tag'] = TagForm()
        context['tags'] = Catblog.objects.all()
        # context['foto_blog'] = MenuFoto.objects.get(menu=5)

        return context


def DetailBlogsView(request, template='blogs/detalle_blog.html', id=None, uri=None):
    nota = get_object_or_404(Blog, id=id)

    lista_tematicas = nota.idcatblog

    try:
        noticias_relacionadas = Blog.objects.filter(idcatblog=lista_tematicas).exclude(id=nota.id).order_by('-id')[:3]
    except:
        pass

    # foto_blog = MenuFoto.objects.get(menu=5)

    return render(request, template, locals())


def blogs_filtrados(request, template="blogs/blog_list.html"):
    object_list = Blog.objects.filter(idcatblog=request.POST["tag"])
    tags = Catblog.objects.all()
    foto_blog = MenuFoto.objects.get(menu=5)
    return render(request, template, locals())
