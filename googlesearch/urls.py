import django
from django.urls import path
from django.views.generic import TemplateView
from .views import cref_cse


urlpatterns = [
    path(
        'results/', 
         TemplateView.as_view(template_name="googlesearch/results.html"),
        name='googlesearch-results'
    ),

    path('cref-cse\.xml/', cref_cse, name='googlesearch-cref-cse'),

]
