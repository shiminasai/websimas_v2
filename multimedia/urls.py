from django.urls import path
from .views import *

urlpatterns = [
	path('galerias-imagenes/', GaleriaImgListView, name = "list-galeria"),
	path('galerias-imagenes/filtro/<id>', filtro_galeria, name = "filtro-galeria"),
	path('galerias-imagenes/<slug>', detalle_galeria, name = "detalle-galeria"),
	path('videos/', videos, name="videos"),
]