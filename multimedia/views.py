from django.shortcuts import render
from .models import *
from django.views.generic import ListView, DetailView
from otras.models import *
from publicaciones.models import *

# Create your views here.

def GaleriaImgListView(request,template='galeria/galeria.html'):
    object_list = GaleriasImg.objects.order_by('-id')
    tematicas = GaleriasImg.objects.values_list('categoria__id','categoria__nombre').distinct('categoria__id').exclude(categoria__nombre = None)
    try:
    	foto_menu = MenuFoto.objects.get(menu=8)
    except:
    	pass

    return render(request, template, locals())

def filtro_galeria(request,id,template='galeria/galeria.html'):
    object_list = GaleriasImg.objects.filter(categoria = id).order_by('-id')
    tematicas = GaleriasImg.objects.values_list('categoria__id','categoria__nombre').distinct('categoria__id').exclude(categoria__nombre = None)
    return render(request, template, locals())

def detalle_galeria(request,slug,template='galeria/detalle.html'):
    object = GaleriasImg.objects.get(slug = slug)
    return render(request, template, locals())

def videos(request,template='galeria/videos.html'):
	object_list = Video.objects.exclude(youtube__exact='').order_by('-id')
	tematicas = Video.objects.exclude(youtube__exact='').values_list('categoria__id','categoria__nombre').distinct('categoria__id').exclude(categoria__nombre = None)

	return render(request, template, locals())