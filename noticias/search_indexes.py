from haystack import indexes
from .models import Noticia
from django.db.models import Q


class NoticiaIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    noticia = indexes.CharField(model_attr='noticia')
    resumen = indexes.CharField(model_attr='resumen', null=True)
    claves = indexes.CharField(model_attr='claves',null=True)
    categoria = indexes.CharField(model_attr='categoria__nombre',null=True)

    def get_model(self):
        return Noticia

    def index_queryset(self, using=None):
        """Used when the entire index for model is updated."""
        return self.get_model().objects.order_by('-id')