from django.contrib import admin
from .models import Aliado, Servicio, Sistema, Financiadores, Autor, Pais, UbicacionTrabajor, Trabajadores, MenuFoto
from portafolio.models import *
from django.contrib.contenttypes.admin  import GenericTabularInline
import nested_admin
# NestedStackedInline, NestedModelAdmin
class FotosInline(nested_admin.NestedGenericTabularInline):
    model = Fotos
    extra = 1

class PortafolioInline(admin.TabularInline):
    # inlines = [FotosInline]
    model = Portafolio

    list_display = ('servicio', 'fecha', 'link')

class ServicioAdmin(admin.ModelAdmin):
	inlines = [PortafolioInline]

class TrabajadoresAdmin(admin.ModelAdmin):
	list_display = ('nombres', 'ubicacion')
	list_filter = ('ubicacion',)

# Register your models here.
admin.site.register(Aliado)
admin.site.register(Servicio)
# admin.site.register(Sistema)
admin.site.register(Financiadores)
admin.site.register(Autor)
admin.site.register(Pais)
admin.site.register(UbicacionTrabajor)
admin.site.register(Trabajadores,TrabajadoresAdmin)
admin.site.register(MenuFoto)
admin.site.register(Categoria)

from solo.admin import SingletonModelAdmin
admin.site.register(ImgIndex,SingletonModelAdmin)
