from django.db import models
from websimas.utils import get_file_path
from sorl.thumbnail import ImageField, get_thumbnail
from ckeditor.fields import RichTextField
from django.template.defaultfilters import slugify
from embed_video.fields import EmbedVideoField
# Create your models here.

class Aliado(models.Model):
    correo = models.CharField(max_length=255, blank=True, null=True)
    direccion = models.CharField(max_length=255, blank=True, null=True)
    descripcion = RichTextField(blank=True, null=True)
    logo = ImageField('Logo oficial', upload_to=get_file_path, blank=True, 
        null=True)
    aliado = models.CharField(max_length=255, blank=True, null=True)
    uri = models.CharField(max_length=500, blank=True, null=True, editable=False)
    web = models.CharField(max_length=300, blank=True, null=True)

    fileDir = 'logosAliados/'

    def save(self, *args, **kwargs):
        self.uri = (slugify(self.aliado))
        super(Aliado, self).save(*args, **kwargs)
    
    def __str__(self):
        return self.aliado

    class Meta:
        verbose_name='Aliado'
        verbose_name_plural='Aliados'


class Servicio(models.Model):
    servicio = models.CharField(max_length=255, blank=True, null=True)
    descripcion = RichTextField(blank=True, null=True)
    imagen = ImageField('Foto', upload_to=get_file_path, blank=True, null=True)
    uri = models.CharField(max_length=500, blank=True, null=True)
    activo = models.BooleanField()

    fileDir = 'fotoServicio/'

    def __str__(self):
        return self.servicio

    class Meta:
        verbose_name='Servicio'
        verbose_name_plural='Servicios'

    def save(self, *args, **kwargs):
        self.uri = (slugify(self.servicio))
        super(Servicio, self).save(*args, **kwargs)

    @property
    def get_thumb(self):
        im = get_thumbnail(self.imagen, '1920x1280', crop='center', quality=99)
        return im.url



class Sistema(models.Model):
    sistema = models.CharField(max_length=255, blank=True, null=True)
    foto = ImageField('Foto sistema', upload_to=get_file_path, blank=True, null=True)
    url = models.CharField(max_length=255, blank=True, null=True)
    fecha = models.DateField(blank=True, null=True)

    fileDir = 'fotoSistema/'

    def __str__(self):
        return self.sistema

    class Meta:
        verbose_name='Sistema'
        verbose_name_plural='Sistemas'


class Financiadores(models.Model):
    logo = models.CharField(max_length=500, blank=True, null=True)
    nombre = models.CharField(max_length=120, blank=True, null=True)
    web = models.CharField(max_length=500, blank=True, null=True)
    orden = models.IntegerField(blank=True, null=True)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name='Financiador'
        verbose_name_plural='Financiadores'


class Autor(models.Model):
    autor = models.CharField(max_length=500, blank=True, null=True)
    email = models.CharField(max_length=150, blank=True, null=True)
    imagen = ImageField(upload_to=get_file_path, blank=True, null=True)
    fileDir = 'autores/'

    def __str__(self):
        return self.autor

    class Meta:
        verbose_name='Autor'
        verbose_name_plural='Autores'



class Pais(models.Model):
    idpais = models.CharField(primary_key=True, max_length=2)
    pais = models.CharField(max_length=80, blank=True, null=True)
    region = models.CharField(max_length=2, blank=True, null=True)
    telefono = models.CharField(max_length=3, blank=True, null=True)
    
    def __str__(self):
        return self.pais

    class Meta:
        verbose_name='Pais'
        verbose_name_plural='Paises'


class UbicacionTrabajor(models.Model):
    ubicacion = models.CharField(max_length=50)
    orden = models.IntegerField(null=True,blank=True)

    class Meta:
        verbose_name_plural = 'Ubicación del trabajador'
        #ordering = ('ubicacion',)


    def __str__(self):
        return self.ubicacion

class Trabajadores(models.Model):
    nombres = models.CharField('Nombres y apellidos', max_length=250)
    foto = ImageField(upload_to=get_file_path, blank=True, null=True)
    correo = models.EmailField(null=True, blank=True)
    cargo = models.CharField('cargo', max_length=50)
    ubicacion = models.ForeignKey(UbicacionTrabajor, on_delete = models.CASCADE)

    fileDir = 'fotoTrabajador/'

    class Meta:
        verbose_name_plural = 'Trabajadores'

    def __str__(self):
        return self.nombres

CHOICE_MENU = (
                (1, "quienes-somos"),
                (2, "publicaciones"),
                (3, "portafolio"),
                (4, "noticias"),
                (5, "blogs"),
                (6, "contactenos"),
                (7, "historia"),
                (8, "galerias-imagenes")
    )

class MenuFoto(models.Model):
    menu = models.IntegerField(choices=CHOICE_MENU)
    titulo = models.CharField(max_length=250)
    foto = ImageField(upload_to=get_file_path)

    fileDir = 'fotoHeader/'

    def __str__(self):
        return self.titulo


class Categoria(models.Model):
    nombre = models.CharField(max_length=350)
    slug = models.SlugField(max_length=350,editable=False)

    class Meta:
        verbose_name_plural = "Categorias"

    def __str__(self):
        return self.nombre

    def save(self, *args, **kwargs):
        self.slug = (slugify(self.nombre))
        super(Categoria, self).save(*args, **kwargs)

from solo.models import SingletonModel
class ImgIndex(SingletonModel):
    menu_principal = ImageField(upload_to=get_file_path, blank=True, null=True)
    cambiando_vidas = ImageField(upload_to=get_file_path, blank=True, null=True)
    proyectos_izquierda = ImageField(upload_to=get_file_path, blank=True, null=True)
    proyectos_derecha = ImageField(upload_to=get_file_path, blank=True, null=True)
    video = ImageField(upload_to=get_file_path, blank=True, null=True)
    url_video = EmbedVideoField(blank=True, null=True)
    suscripcion = ImageField(upload_to=get_file_path, blank=True, null=True)
    fileDir = 'img-index/'

    def __str__(self):
        return "Configuración index"

    class Meta:
        verbose_name = "Configuración index"
