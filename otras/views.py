from django.shortcuts import render, get_object_or_404
from .models import *
from portafolio.models import *

# Create your views here.

def servicios(request,template='servicios/servicios.html', uri=None):
	servicio = get_object_or_404(Servicio,uri=uri)
	portafolio = Portafolio.objects.filter(idservicio = servicio).order_by('-id')
	object_list = Servicio.objects.filter(activo = True).order_by('-id')
	foto_portafolio = MenuFoto.objects.get(menu=3)
	return render(request, template, locals())