from django.shortcuts import render
from .models import *

# Create your views here.
def list_proyectos(request,template='proyectos/list_proyectos.html'):
	object_list = Proyecto.objects.order_by('-id')
	return render(request, template, locals())

def detalle_proyecto(request,template='proyectos/proyecto_detalle.html',slug=None):
	object = Proyecto.objects.get(slug = slug)
	return render(request, template, locals())