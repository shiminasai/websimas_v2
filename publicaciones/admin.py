from django.contrib import admin
from .models import *

# class PublicacionResource(ModelResource):
#     fecha = fields.Field(column_name="fecha",widget=DateWidget)
#     full_title = fields.Field()

#     class Meta:
#         model = Publicacion

#         fields = ('publicacion','fecha','codigo','autor','resumen',
#                   'claves','lugar','notas', 'tipo_publicacion', 'edicion',
#                   'editorial', 'paginas', 'isbn','full_title')

#     def dehydrate_full_title(self, publicacion):
#         return '%s' % (publicacion.fecha)



class PublicacionAdmin(admin.ModelAdmin):
    #resource_class = PublicacionResource
    search_fields = ('id','publicacion', 'autor', 'editorial','claves','resumen',)
    list_filter = ('idcategoria', 'tipo_publicacion', 'cidoc','iddisponibilidad')
    list_display = ('id', 'publicacion', 'codigo', 'autor', 'cidoc','editorial','categorias','fecha')
    list_display_links = ('id', 'publicacion')
    fieldsets = (
         (None, {
             'fields': (('publicacion', 'portada'), ('fecha', 'codigo', 'autor'),
     					'resumen', ('claves', 'lugar'),'idcategoria', 'notas', ('cidoc','edicion',
     					'editorial'), ('paginas', 'isbn', 'tipo_publicacion'), ('precio',
     					'iddisponibilidad', 'enportada','tipo'))
         			}),
         ('Archivos para adjuntar', {
             'fields': (('nombre1', 'archivo1'), ('nombre2','archivo2'),
                       ('nombre3', 'archivo3'),('nombre4', 'archivo4'),
    					('nombre5', 'archivo5'))
         }),
    )

    filter_horizontal = ['idcategoria',]
    # ordering = ['-fecha']

# Register your models here.
admin.site.register(Tematica)
admin.site.register(Tipopublicacion)
admin.site.register(Publicacion, PublicacionAdmin)
# admin.site.register(Disponibilidad)