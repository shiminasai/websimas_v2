from haystack import indexes
from .models import Publicacion
from django.db.models import Q


class PublicacionIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    publicacion = indexes.CharField(model_attr='publicacion')
    # resumen = indexes.CharField(model_attr='resumen')
    # fecha = indexes.DateField(model_attr='fecha',null=True)
    # editorial = indexes.CharField(model_attr='editorial',null=True)
    idcategoria = indexes.MultiValueField(model_attr='idcategoria__nombre',null=True)
    # edicion = indexes.CharField(model_attr='edicion',null=True)
    # claves = indexes.CharField(model_attr='claves',null=True)

    def get_model(self):
        return Publicacion

    def prepare_idcategoria(self, object):
        return [categoria.nombre for categoria in object.idcategoria.all()]

    def index_queryset(self, using=None):
        """Used when the entire index for model is updated."""
        return self.get_model().objects.filter(iddisponibilidad=4).order_by('-fecha')