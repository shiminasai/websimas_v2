from django.urls import path, include
from .views import *

app_name = 'publicaciones'

urlpatterns = [
    path('publicaciones/', ListBooksView, name ='publicaciones'),
    path('publicaciones/filtro/<id>', FiltroListBooksView, name ='filtro-publicaciones'),
    path('publicaciones/<int:id>/<str:uri>/', DetailBooksView, name ='detalle-publicacion'),
    # path('publicaciones/simas/', ListBookSimasView.as_view(), name ='publicaciones'),
    # path('publicaciones/cidoc/', ListBookCidocView.as_view(), name ='publicaciones'),
    path('publicaciones/categoria/<str:slug>', publicaciones_categoria, name = "list-publicaciones-cat")
]



""" urlpatterns = patterns(
    'publicaciones.views',
    url(r'^publicaciones/$', ListBooksView.as_view(),
        name='publicaciones'),
    url(r'^publicaciones/(?P<id>[0-9]+)/(?P<uri>[-\w]+)/$',
        'DetailBooksView', name='detalle-publicacion'),
    url(r'^publicaciones/simas/$', ListBookSimasView.as_view(),
        name='publicaciones'),
    url(r'^publicaciones/cidoc/$', ListBookCidocView.as_view(),
        name='publicaciones'),
)
 """