from __future__ import absolute_import, unicode_literals
from celery import shared_task,Task
from django.core.mail import send_mail, EmailMultiAlternatives
from django.template.loader import render_to_string
from noticias.models import *
from publicaciones.models import *
from multimedia.models import *
from suscriptores.models import *

class CallbackTask(Task):
	def on_success(self, retval, task_id, args, kwargs):
		subject, from_email = 'Boletin SIMAS enviado con exito', 'simas@gmail.com'
		text_content = render_to_string('email/boletin_enviado.txt')

		html_content = render_to_string('email/boletin_enviado.txt')

		list_mail = ['erick@simas.org.ni',]
		#'carlos@simas.org.ni','manejo-informacion@simas.org.ni','coordinacion@simas.org.ni'

		msg = EmailMultiAlternatives(subject, text_content, from_email, list_mail)
		msg.attach_alternative(html_content, "text/html")
		msg.send()

@shared_task(default_retry_delay=10, max_retries=3, base=CallbackTask)
def boletin():
	notas = Noticia.objects.order_by('-id')[:3]
	videos = Video.objects.exclude(youtube__exact='').order_by('-id')[:2]
	publicaciones = Publicacion.objects.order_by('-id')[:2]
	subject, from_email = 'Boletín SIMAS', 'simas@gmail.com'
	text_content = render_to_string('email/boletin.txt', {'notas': notas, 'videos': videos, 'publicaciones': publicaciones})

	html_content = render_to_string('email/boletin.txt', {'notas': notas, 'videos': videos, 'publicaciones': publicaciones})

	list_mail = Correo.objects.values_list('email', flat=True)

	msg = EmailMultiAlternatives(subject, text_content, from_email, list_mail)
	msg.attach_alternative(html_content, "text/html")
	msg.send()
