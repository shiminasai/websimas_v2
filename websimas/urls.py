"""websimas URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf.urls.static import static
from django.conf import settings
from noticias.views import contacto_ajax, get_deptos, search_notas, get_img_servicios,get_blogs,list_portfolio,filtro_portafolio,detalle_portafolio
from django.conf.urls import url
from django.views.generic import TemplateView
from otras.views import *
from proyectos.views import *
from publicaciones.views import *

admin.site.site_header = "Administración SIMAS"
admin.site.site_title = "Administración SIMAS"

urlpatterns = [

    path('', include(('publicaciones.urls','publicaciones'), namespace='publicaciones')),
    path('', include(('noticias.urls','noticias'), namespace='noticias')),
    path('', include(('blogs.urls','blogs'), namespace='blogs')),
    path('busqueda/', include(('googlesearch.urls','googlesearch'), namespace='googlesearch')),    
    path('contacto_ajax/', contacto_ajax, name='contacto_ajax'),
    path('admin/', admin.site.urls),
    path('captcha/', include('captcha.urls')),
    path('ckeditor/', include('ckeditor_uploader.urls')),
    path('_nested_admin/', include('nested_admin.urls')),
    path('', include(('multimedia.urls','multimedia'), namespace='multimedia')),
    path('pages/', include('django.contrib.flatpages.urls')),
    path('servicios/<str:uri>/', servicios, name='servicios'),
    # path('publicaciones/busqueda/', include('haystack.urls')),
    path('publicaciones/busqueda/', search_publicacion, name = 'search-publicacion'),
    path('notas/busqueda/', search_notas, name = 'search-notas'),
    path('ajax/deptos/', get_deptos, name='get-deptos'),
    path('proyectos/', list_proyectos, name = 'list-proyectos'),
    path('proyectos/<str:slug>/', detalle_proyecto, name = 'detalle-proyecto'),
    path('ajax/servicios/', get_img_servicios, name='get-img-servicios'),
    path('ajax/blogs/', get_blogs, name='get-blogs'),
    path('portafolio/', list_portfolio, name='list-portafolio'),
    path('portafolio/<slug>', detalle_portafolio, name='detalle-portafolio'),
    path('portafolio/filtro/<id>', filtro_portafolio, name='filtro-portafolio'),
]


if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL , document_root = settings.STATIC_ROOT )
    urlpatterns += static(settings.MEDIA_URL , document_root = settings.MEDIA_ROOT )
